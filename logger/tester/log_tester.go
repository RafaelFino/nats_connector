package main

import (
	log "c6bank.com/C6libs/logger"
	"fmt"
	"os"
	"time"
)

type TestValues struct {
	Field1 string
	Field2 string
	Value1 int
	Value2 int
}

type TestValues2 struct {
	Field3 string
	Field4 string
	Value3 int
	Value4 int
}

var logger *log.Logger

func main(){
	os.Setenv("HOST_ID", "macOS.fino")

	//Init logger service
	logConfig := log.CreateConfig("Tester.Logger")

	logConfig.IncludeTracer = true

	log.InitWithConfig(logConfig)

	//Get current logger instance
	logger = log.GetInstance()
	l := logger

	wait, _ := time.ParseDuration("50ms")

	fmt.Println("### Start...")
	logger.Info("---- Log from main!")

	arg := TestValues{Field1: "xpto", Field2:"blá", Value1:1, Value2: 100}
	context1 := logger.CreateContext(arg)

	fmt.Println("---- CONTEXT1")
	Scenario(wait, context1, nil)

	//tags
	details := log.CreateLogDetails()
	details.AddTag("tag1")
	details.AddTag("tag2")
	details.AddTag("tag3")

	//details
	details.AddDetail("event1", "value1")
	details.AddDetail("event2", "value2")
	details.AddDetail("event3", "value3")
	fmt.Println("---- CONTEXT1 - With tags and events")
	Scenario(wait, context1, details)

	//More args
	arg2 := TestValues2{Field3: "qwerty", Field4:"Búúúú", Value3:314, Value4:768}
	context2 := logger.CreateContext(arg, arg2)
	fmt.Println("---- CONTEXT2")
	Scenario(wait, context2, details)

	//Bulk insert tags
	details2 := log.CreateLogDetails()
	logger.AddDefaultTag("DefaultTag1")
	details2.AddTags([]string{ "tag4", "tag5", "tag6"})

	//Bulk insert events
	logger.AddDefaultDetail("DefaultEvent1", "DefaultValue1")
	details2.AddDetails(map[string]string{"event4": "value4", "event5":"value5", "event6":"value6"})
	fmt.Println("---- CONTEXT2 - With tags and events")
	Scenario(wait, context2, details2)

	//Clear details
	fmt.Println("---- CONTEXT2 - Clear details")
	Scenario(wait, context2, details2)

	fmt.Println("### Stop!")

	l.Info("STOP!")
}

func Scenario(wait time.Duration, context *log.LogContext, detail *log.LogDetails) {
	fmt.Println("---- Running batch...")
	logger.Info("---- Log from main.Scenario!")
	if detail == nil {
		detail = log.CreateLogDetails()
	}

	//stdout
	fmt.Println("---- STDOUT - TEXT")
	detail.AddDetail("test_type", "stdout")
	detail.AddDetail("formatter", "text")
	logger.SetStdout()
	logger.SetTextFormatter()

	SendLog(2, wait, context, detail)

	fmt.Println("---- STDOUT - JSON")
	logger.SetJsonFormatter()
	detail.AddDetail("formatter", "json")
	SendLog(2, wait, context, detail)

	//file
	fmt.Println("---- FILE - TEXT")
	detail.AddDetail("test_type", "file")
	detail.AddDetail("formatter", "text")
	logger.SetTextFormatter()
	logger.SetOutputFile("/tmp/log_test.text")
	SendLog(2, 0, context, detail)

	fmt.Println("---- FILE - JSON")
	detail.AddDetail("formatter", "json")
	logger.SetJsonFormatter()
	logger.SetOutputFile("/tmp/log_test.json")
	SendLog(2, 0, context, detail)
}

func SendLog(qty int, wait time.Duration, context *log.LogContext, detail *log.LogDetails) {
	logger.Info("---- Log from main.SendLog!")

	levels := []log.Level { log.InfoLevel, log.DebugLevel, log.WarnLevel, log.ErrorLevel }

	for count := 0; count < qty; count++ {
		logger.Debug("simple Debug message")
		logger.DebugContext(context,"Debug message with context - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)
		logger.DebugDetails(context, detail,"Debug message with detail - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)

		time.Sleep(wait)

		logger.Info("simple Info message")
		logger.InfoContext(context,"Info message with context - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)
		logger.InfoDetails(context, detail,"Info message with detail - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)

		time.Sleep(wait)

		logger.Warning("simple Warning message")
		logger.WarningContext(context,"Warning message with context - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)
		logger.WarningDetails(context, detail,"Warning message with detail - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)

		time.Sleep(wait)

		logger.Error("simple Error message")
		logger.ErrorContext(context,"Error message with context - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)
		logger.ErrorDetails(context, detail,"Error message with detail - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)

		time.Sleep(wait)

		for _, level := range levels {
			logger.Send(level, nil, "simple Error message")
			logger.SendContext(context,level, detail, "Error message with context - args: stringValue: [%s] decimalValue: [%d]", "xpto_bla_bla_bla", count)
		}

		fmt.Printf("\tStep %d\n", count)
	}
}