package logger

type Config struct {
	ServiceName    string            `json:"service_name"`
	OutputFilePath string            `json:"log_path,omitempty"`
	LogLevel       Level             `json:"log_level,omitempty"`
	HostID         string            `json:"host_id,omitempty"`
	TextFormatter  bool              `json:"text_formatter,omitempty"`
	IncludeTracer  bool              `json:"tracer,omitempty"`
	DefaultDetails LogDetails       `json:"Details,omitempty"`
}

func CreateConfig(serviceName string) *Config {
	return &Config{ ServiceName: serviceName, TextFormatter:true, LogLevel:DebugLevel, HostID:ReadHostId(), IncludeTracer:true }
}