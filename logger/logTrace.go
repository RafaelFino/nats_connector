package logger

import (
	"fmt"
	"runtime"
	"strings"
)

type LogTrace struct {
	Ip      []string `json:"ip,omitempty"`
	Host    string   `json:"host,omitempty"`
	Service string   `json:"service,omitempty"`
	Caller  string   `json:"caller"`
}

func traceMethod() string {
	pc := make([]uintptr, 4)
	n := runtime.Callers(3, pc)
	if n > 0 {
		pc = pc[:n]
	}

	frames := runtime.CallersFrames(pc)

	for {
		frame, _ := frames.Next()

		if !strings.Contains(frame.File, "logger.go") {
			return fmt.Sprintf("%s[%d]", frame.Function, frame.Line)
		}
	}

	return runtime.FuncForPC(pc[0]).Name()
}