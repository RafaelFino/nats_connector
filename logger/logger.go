package logger

import (
	"encoding/json"
	"fmt"
	"github.com/oklog/ulid"
	log "github.com/sirupsen/logrus"
	"math/rand"
	"net"
	"os"
	"regexp"
	"time"
)

type Logger struct {
	Ip      []string
	Host    string
	Service string
	logger  *log.Logger

	DefaultDetails LogDetails

	isTextFormatter bool
	includeTracer bool
	entropySeed *rand.Rand
}

type Level uint32

const (
	PanicLevel Level = iota
	FatalLevel
	ErrorLevel
	WarnLevel
	InfoLevel
	DebugLevel
)

const defaultLocalIp =  "0.0.0.0"
var instance *Logger

func Init(serviceName string) {
	InitWithConfig(CreateConfig(serviceName))
}

func InitWithConfig(config *Config) {
	instance = builder(config)
}

func GetInstance() *Logger {
	if instance == nil {
		//Missing service name!
		Init(os.Args[0])
	}

	return instance
}

func builder(config *Config) *Logger {
	ret := new(Logger)

	ret.logger = &log.Logger{}

	ret.Ip = getLocalIp()
	ret.Host = config.HostID
	ret.Service = config.ServiceName
	ret.entropySeed = rand.New(rand.NewSource(time.Now().UnixNano()))
	ret.isTextFormatter = config.TextFormatter
	ret.includeTracer = config.IncludeTracer

	if config.DefaultDetails.IsEmpty() {
		ret.DefaultDetails = LogDetails{ Tags: make([]string, 0), Data: make(map[string]string)}
	} else {
		ret.DefaultDetails = config.DefaultDetails
	}

	if config.TextFormatter {
		ret.SetTextFormatter()
	} else {
		ret.SetJsonFormatter()
	}

	if len(config.OutputFilePath) != 0 {
		ret.SetOutputFile(config.OutputFilePath)
	} else {
		ret.SetStdout()
	}

	ret.SetLogLevel(config.LogLevel)

	return ret
}

func (l *Logger) GetInternalLogger() interface{} {
	return l.logger
}

func (l *Logger) SetConfig(config *Config) {
	instance = builder(config)
}

var fixJsonArg = regexp.MustCompile("\"")

func (l *Logger)formatLogField(value interface{}) interface{} {
	if l.isTextFormatter {
		jsonData, err := json.Marshal(value)
		if err != nil {
			return fmt.Sprintf("%v", value)
		}

		return fixJsonArg.ReplaceAllString(string(jsonData), "")
	}
	return value
}

func (l *Logger) CreateID() string {
	return ulid.MustNew(ulid.Now(), l.entropySeed).String()
}

func (l *Logger) AddDefaultDetail(info string, value string) {
	l.DefaultDetails.Data[info] = value
}

func (l *Logger) AddDefaultDetails(info map[string]string) {
	for k,v := range info {
		l.DefaultDetails.Data[k] = v
	}
}

func (l *Logger) AddDefaultTag(tag string) {
	l.DefaultDetails.Tags = append(l.DefaultDetails.Tags, tag)
}

func (l *Logger) SetLogLevel(level Level) {
	l.logger.Level = log.Level(level)
}

func (l *Logger) SetJsonFormatter() {
	l.logger.Formatter = &log.JSONFormatter{}
	l.isTextFormatter = false
}

func (l *Logger) SetTextFormatter() {
	l.logger.Formatter = &log.TextFormatter{ForceColors:true, FullTimestamp: true}
	l.isTextFormatter = true
}

func (l *Logger) SetStdout() {
	l.logger.Out = os.Stdout
}

func (l *Logger) SetOutputFile(path string) {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)

	if err == nil {
		l.logger.Out = file
		l.Debug("[Logger] Set output to file [%s]", path)
	}
}

func (l *Logger) CreateContext(arg ...interface{}) *LogContext {
	return l.BuildContextFromID(l.CreateID(), arg...)
}

func (l *Logger) BuildContextFromID(id string, args ...interface{}) *LogContext {
	context := &LogContext{Id: id, start: time.Now()}

	context.Args = args

	return context
}

func (l *Logger) makeEntry() *log.Entry {
	if l.includeTracer {
		trace := &LogTrace{Service: l.Service, Caller: traceMethod(), Ip: l.Ip, Host: l.Host}

		return l.logger.WithFields(log.Fields{
			"trace": l.formatLogField(trace),
		})
	} else {
		return l.logger.WithFields(log.Fields{})
	}
}

func (l *Logger) makeContextEntry(context *LogContext) *log.Entry {
	entry := l.makeEntry()

	context.Duration = time.Since(context.start).String()
	ret := entry.WithField(	"context", l.formatLogField(context))

	return ret
}

func (l *Logger) insertDetails(entry *log.Entry, details *LogDetails) *log.Entry {
	details = AppendDetails(details, l.DefaultDetails)

	if details != nil && !details.IsEmpty(){
		details.FixUniqueTags()
		return entry.WithField("details", l.formatLogField(details))
	}

	return entry
}

func getLocalIp() []string {
	var ret []string

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return append(ret, defaultLocalIp)
	}

	for _, add := range addrs {
		if ipnet, ok := add.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ret = append(ret, ipnet.IP.String())
			}
		}
	}

	if len(ret) == 0 {
		return append(ret, defaultLocalIp)
	}

	return ret
}

func ReadHostId() string {
	ret := os.Getenv("HOST_ID")

	return ret
}

func (l *Logger) Send(level Level, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(l.CreateContext(), level, details, message, args...)
}

func (l *Logger) SendValues(values interface{}, level Level, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(l.CreateContext(values), level, details, message, args...)
}

func (l *Logger) SendContext(context *LogContext, level Level, details *LogDetails, message string, args ...interface{}) *LogContext {
	entry := l.insertDetails(l.makeContextEntry(context), details)

	switch level {
		case InfoLevel:
			entry.Infof(message, args...)
		case DebugLevel:
			entry.Debugf(message, args...)
		case WarnLevel:
			entry.Warningf(message, args...)
		case ErrorLevel:
			entry.Errorf(message, args...)
		case FatalLevel:
			entry.Fatalf(message, args...)
		case PanicLevel:
			entry.Panicf(message, args...)
	}

	return context
}

func (l *Logger) Info(message string, a ...interface{}) *LogContext {
	return l.Send(InfoLevel, nil, message, a...)
}

func (l *Logger) InfoValues(values interface{}, message string, a ...interface{}) *LogContext {
	return l.SendValues(values, InfoLevel, nil, message, a...)
}

func (l *Logger) InfoContext(context *LogContext, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, InfoLevel, nil, message, args...)
}

func (l *Logger) InfoDetails(context *LogContext, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, InfoLevel, details, message, args...)
}

func (l *Logger) Debug(message string, a ...interface{}) *LogContext {
	return l.Send(DebugLevel, nil, message, a...)
}

func (l *Logger) DebugValues(values interface{}, message string, a ...interface{}) *LogContext {
	return l.SendValues(values, DebugLevel, nil, message, a...)
}

func (l *Logger) DebugContext(context *LogContext, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, DebugLevel, nil, message, args...)
}

func (l *Logger) DebugDetails(context *LogContext, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, DebugLevel, details, message, args...)
}

func (l *Logger) Warning(message string, a ...interface{}) *LogContext {
	return l.Send(WarnLevel, nil, message, a...)
}

func (l *Logger) WarningValues(values interface{}, message string, a ...interface{}) *LogContext {
	return l.SendValues(values, WarnLevel, nil, message, a...)
}

func (l *Logger) WarningContext(context *LogContext, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, WarnLevel, nil, message, args...)
}

func (l *Logger) WarningDetails(context *LogContext, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, WarnLevel, details, message, args...)
}

func (l *Logger) Error(message string, a ...interface{}) *LogContext {
	return l.Send(ErrorLevel, nil, message, a...)
}

func (l *Logger) ErrorValues(values interface{}, message string, a ...interface{}) *LogContext {
	return l.SendValues(values, ErrorLevel, nil, message, a...)
}

func (l *Logger) ErrorContext(context *LogContext, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, ErrorLevel, nil, message, args...)
}

func (l *Logger) ErrorDetails(context *LogContext, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, ErrorLevel, details, message, args...)
}

func (l *Logger) Fatal(message string, a ...interface{}) *LogContext {
	return l.Send(FatalLevel, nil, message, a...)
}

func (l *Logger) FatalValues(values interface{}, message string, a ...interface{}) *LogContext {
	return l.SendValues(values, FatalLevel, nil, message, a...)
}

func (l *Logger) FatalContext(context *LogContext, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, FatalLevel, nil, message, args...)
}

func (l *Logger) FatalDetails(context *LogContext, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, FatalLevel, details, message, args...)
}

func (l *Logger) Panic(message string, a ...interface{}) *LogContext {
	return l.Send(PanicLevel, nil, message, a...)
}

func (l *Logger) PanicValues(values interface{}, message string, a ...interface{}) *LogContext {
	return l.SendValues(values, PanicLevel, nil, message, a...)
}

func (l *Logger) PanicContext(context *LogContext, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, PanicLevel, nil, message, args...)
}

func (l *Logger) PanicDetails(context *LogContext, details *LogDetails, message string, args ...interface{}) *LogContext {
	return l.SendContext(context, PanicLevel, details, message, args...)
}