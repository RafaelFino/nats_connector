package logger

type LogDetails struct {
	Tags []string          `json:"tags,omitempty"`
	Data map[string]string `json:"data,omitempty"`
}

func CreateLogDetails() *LogDetails {
	return &LogDetails{ Tags: make([]string, 0), Data: make(map[string]string)}
}

func (d *LogDetails) IsEmpty() bool {
	if d == nil {
		return true
	}

	return len(d.Tags) == 0 && len(d.Data) == 0
}

func (d *LogDetails) AddTag(tag string) {
	if d.Tags == nil {
		d.Tags = make([]string, 0)
	}

	d.Tags = append(d.Tags, tag)
}

func (d *LogDetails) AddTags(tags []string) {
	if d.Tags == nil {
		d.Tags = make([]string, 0)
	}

	d.Tags = append(d.Tags, tags...)
}

func (d *LogDetails) AddDetail(key string, value string) {
	if d.Data == nil {
		d.Data = make(map[string]string)
	}
	d.Data[key] = value
}

func (d *LogDetails) AddDetails(data map[string]string) {
	if d.Data == nil {
		d.Data = make(map[string]string)
	}

	for key,value := range data {
		d.Data[key] = value
	}
}

func (d *LogDetails) FixUniqueTags() {
	if len(d.Tags) > 1 {
		set := make(map[string]bool)
		var tags []string

		for _, t := range d.Tags {
			set[t] = true
		}

		for k := range set {
			tags = append(tags, k)
		}

		d.Tags = tags
	}
}

func AppendDetails(target *LogDetails, details LogDetails) *LogDetails {
	if target == nil || target.IsEmpty() {
		return &details
	}

	if details.Tags != nil && len(details.Tags) > 0 {
		target.Tags = append(target.Tags, details.Tags...)
	}

	if details.Data != nil && len(details.Data) > 0 {
		if target.Data == nil {
			target.Data = make(map[string]string)
		}

		for k, v := range details.Data {
			target.Data[k] = v
		}
	}

	return target
}