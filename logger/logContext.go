package logger

import "time"

type LogContext struct {
	Id       string      `json:"id"`
	Args     interface{} `json:"args,omitempty"`
	Duration string      `json:"duration"`
	start    time.Time
}

func (c *LogContext) SetCreationDate(when time.Time) {
	c.start = when
}