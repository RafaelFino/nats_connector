# C6 Logger

## Logs
As aplicações devem enviar seus logs para um local comum (syslog) e de forma assincrona, um coletor deve enviar esses dados para um recurso de infra estrutura externo ao host onde o serviço está hospedado

Os logs de aplicações devem ser armazenados seguindo as regras de compliance e auditoria da instituição
Os logs devem ter forma padronizada, seguindo os padrões aqui definidos
Informações sobre os clientes devem ser mascaradas para evitar furos de segurança
As informações de log devem ser relevantes e reportar o comportamento da aplicação, afim de possibilitar uma análise apenas por essas informações

## Padrões de log
Cada aplicação deve seguir o seguinte modelo para geração de log:
```json
Exemplo de log
{
    "time": "2009-09-01T00:00:00.000Z",
    "level": "critic",
    "trace":{
        "ip": "0.0.0.0",
        "host": "AWS_CONTAINER_007_SERVICE_XPTO",
        "service": "CORE",
        "method": "Balance.GET"
    },
    "context":{
        "Id": "01CN2CKAYDEP9WP6S8YFYXQ4HH",
        "Args": {
            "account": "####",
            "total": "8000000000",
            "avaliable": "5444333222.111"
        }
        "duration":"12.806µs"
    }
    "msg": "Client balance request"
}
```

### Onde:
|Campo | Descrição |
| --- | --- |
|TIME | Data do evento a ser logado, UTC |
|LEVEL | Nível de log da mensagem |
|MSG | Mensagem com informações relevantes e que podem rastrear o evento reportado (adequada as políticas de segurança para dados sigilosos) |
|TRACER.HOST | Identificador do host ou container que gerou a mensagem de log
|TRACER.IP | Endereço IP de origem do serviço
|TRACER.SERVICE | Nome do serviço que está reportando o comportamento
|TRACER.METHOD | Método e classe dentro do serviço com o comportamento reportado
|CONTEXT.ID | (opcional) Identificador único (ulid) da requisição que gerou as mensagens de log (agrupar por requisição para rastreabilidade)
|CONTEXT.ARGS | (opcional) Dados relevantes a mensagem reportada no formato JSON (adequada as políticas de segurança para dados sigilosos)
|CONTEXT.DURATION |  (opcional) Tempo de vida do contexto


### Níveis de log
|Level | Abreviação | ID | Situação |
| :---: |  :---: |  :---: |  --- | 
|PANIC | panic | 0 | Comportamento inesperado que deve imediatamente suspender a execução do sistema
|CRITIC | fatal | 1 | Comportamento inesperado impede o procedimento de continuar
|ERROR | error | 2 | Comportamento inesperado que deve ser reportado e possivelmente tratado ou retornar falha no processamento
|WARNING | warning | 3 | Comportamento fora do esperado mas contornável e a operação pode continuar
|INFO | info | 4 | Notificação relevante para rastreabilidade de comportamento de negócio esperado
|DEBUG | debug | 5 | Informações relevantes para análise de comportamento do serviço


## Formato
Todas as informações de log serão transportadas e armazenadas, a preocupação com o tamanho, objetividade e padrão dessas mensagens é fundamental para tornar esse processo o mais eficiente possível e nos ajudar a extrair métricas e ter visibilidade do funcionamento de todas as nossas plataformas.

O bloco de tracer já fornece dados de rastreabilidade, portanto detalhes como classe e método onde o processamento ocorreram, podem ser omitidos das mensagens de log.

### Trace
```go
type LogTrace struct {
   Ip         []string
   Host       string
   Service    string
   Method     string
}
```

Como boa prática e para permitir o uso desses registros para relatórios, todos esses dados serão indexados em bases de busca, um padrão dessas mensagens torna esses processos muito mais eficientes, portanto é fortemente recomendado que sejam utilizados os seguintes termos para as determinadas situações:

Exemplo de utlização do SDK padrão para log escrito na linguagem GO:

### Uso do SDK de log (GO)
```go
func RequestX(par1 string, par2 string) ResponseY {
   var logger = log.New("LogExample")
 
   //Example method args:
   args := argxX{Par1: par1, Par2: par2}
 
   //Create a log context with an ULID
   logContext := logger.BuildContext()
 
   //Change log context values
   logContext.Args = args
 
   // Send an info message:
   //Simple debug message
   logger.Debug("requesting data") //poor
   //Formatted debug message
   logger.Debugf("requesting data from=%s", serverUrl)
   //Debug message with values
   logger.DebugContext(logContext, "requesting data")
   //Formatted debug message with values
   logger.DebugContextf(logContext, "requesting data from=%s", serverUrl) //recommended
 
   //Example method call:
   response, err := resource.Request(serverUrl, args)
 
   //check error
   if err != nil {
      // Send an error message:
      //Simple error message
      logger.Error("Request fail") //poor
      //Formatted error message
      logger.Error("Request fail error=%s", err)
      //Error message with values
      logger.ErrorContext(logContext, "Request fail") //poor
      //Formatted error message with values
      logger.DebugContextf(logContext, "Request fail error=%s", err) //recomended
   } else {
      //Change log context values
      logContext.ContextValues = response
      if response.status == RequestStatus.Warning {
         // Send an warning message:
         //Simple warning message
         logger.Warning("warning") //poor
         //Formatted warning message
         logger.Warningf("warning alert=%v", response.AlertMessage) //redundant
         //Warning message with values
         logger.WarningContext(logContext, "warning") //recommended
         //Formatted warning message with values
         logger.WarningContextf(logContext, "warning alert=%v", response.AlertMessage) //redundant
      } else {
         // Send an info message:
         //Simple info message
         logger.Info("ok")
         //Formatted info message
         Logger.Info("ok args=%v", args)
         //Info message with values
         logger.InfoContext(logContext, "ok")
         //Formatted info message with values
         logger.InfoContextf(logContext, "ok args=%v", args) //recommended
      }
   }
 
   return response
}
```
