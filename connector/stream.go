package nats_connector

import (
	log "c6bank.com/C6libs/logger"
	"encoding/json"
	"fmt"
	"github.com/nats-io/go-nats"
	"github.com/nats-io/go-nats-streaming"
	"time"
)

func NewStreamConnector() *StreamConnection {
	ret := &StreamConnection{ Subscriptions:  make(map[string]StreamSubscriptionInfo)}
	ret.Logger = log.GetInstance()

	return ret
}

func (n *StreamConnection) Connect(serverUrl string, clusterId string, clientId string) (stan.Conn, error) {
	return n.ConnectTLS(serverUrl, clusterId, clientId, NatsSecureInfo{})
}

func (n *StreamConnection) ConnectTLS(serverUrl string, clusterId string, clientId string, tlsInfo NatsSecureInfo) (stan.Conn, error) {
	n.Logger.Debug("try to connect on [%s] => ClientId [%s]", clusterId, clientId)

	nc, err := n.CreateNatsConnection(serverUrl, tlsInfo, n.handleNatsConnectionLost)

	if err != nil {
		return nil, fmt.Errorf("error to connect on nats-stream server [serverUrl: %s] [clusterId: %s] [clientId: %s] => %s", serverUrl, clusterId, clientId, err)
	}

	opts := []stan.Option{
		stan.NatsConn(nc),
		stan.NatsURL(serverUrl),
		stan.ConnectWait(10 * time.Second),
		stan.SetConnectionLostHandler(n.handleStreamConnectionLost),
	}

	sc, err := stan.Connect(clusterId, clientId, opts...)
	if err != nil {
		return nil, fmt.Errorf("error to connect on nats-stream server [serverUrl: %s] [clusterId: %s] [clientId: %s] => %s", serverUrl, clusterId, clientId, err)
	}

	n.Logger.Debug("connected on nats-streram [serverUrl: %s] [clusterId: %s] => ClientId [%s]", serverUrl, clusterId, clientId)

	n.StreamId = clusterId
	n.ServerUrl = serverUrl
	n.ClientId = clientId
	n.Conn = sc

	return sc, err
}

func (n *StreamConnection) Close() {
	n.Unsubscribe()
	n.Conn.Close()
}

func (n *StreamConnection) Unsubscribe() {
	for _, subs := range n.Subscriptions {
		subs.Subscription.Unsubscribe()
	}

	n.Subscriptions = nil
}

func (n *StreamConnection) UnsubscribeSubject(subject string) {
	for _, subs := range n.Subscriptions {
		subs.Subscription.Unsubscribe()
	}

	n.Subscriptions[subject].Subscription.Unsubscribe()
	delete(n.Subscriptions, subject)
}

func (n *StreamConnection) TryReconnect() {
	var err error

	n.Logger.Info("trying to reconnect on [%s]", n.ServerUrl)

	if n.IsTLS() {
		_, err = n.ConnectTLS(n.ServerUrl,  n.StreamId, n.ClientId, n.TLSInfo)
	} else {
		_, err = n.Connect(n.ServerUrl, n.StreamId, n.ClientId)
	}

	if err != nil {
		n.Logger.ErrorValues(err,"fail to reconnect - Error: [%s]", err)
		panic(err)
	}
}

func (n *StreamConnection) handleNatsConnectionLost(conn *nats.Conn) {
	n.Logger.ErrorValues(conn, "nats-server connection lost, trying to reconnect... ")

	n.TryReconnect()

	for subject, subs := range n.Subscriptions {
		n.Logger.Info("trying to subscribe on subject [%s]", subject)

		err := n.Subscribe(subs.Subject, subs.Callback, subs.ErrorCallback)

		if err != nil {
			n.Logger.Error("fail try to subscribe on [%s] => Error: %s", subject, err)
		}
	}
}

func (n *StreamConnection) handleStreamConnectionLost(conn stan.Conn, err error) {
	args := map[string]interface{} {"conn": conn, "error": err}
	n.Logger.ErrorValues(args, "stream-server connection lost, trying to reconnect... ")
	n.handleNatsConnectionLost(conn.NatsConn())
}

func (n *StreamConnection) IsConnected() bool {
	return  n.Conn.NatsConn().IsConnected()
}

func (n *StreamConnection) IsConnecting() bool {
	status := n.Conn.NatsConn().Status()
	if status == nats.RECONNECTING || status == nats.CONNECTING {
		return true
	}
	return false
}

func (n *StreamConnection) checkConnection(wait bool) {
	conn := n.Conn.NatsConn()
	if conn.IsConnected() {
		return
	}

	n.Logger.Warning("server is not connected...")

	if conn.IsClosed() {
		n.Logger.Debug("connection is closed! Trying to reconnect...")
		n.TryReconnect()
		n.checkConnection(wait)
	}

	if wait {
		for conn.IsReconnecting() {
			n.Logger.Debug("in reconnection state, waiting...")
			wait, _ := time.ParseDuration("1s")
			time.Sleep(wait)
		}

		n.checkConnection(wait)
	}
}

func (n *StreamConnection) Publish(subject string, message []byte) error {
	n.checkConnection(true)

	err := n.Conn.Publish(subject, message)

	n.Logger.DebugValues(map[string]interface{} {"subject": subject, "message-size": len(message)},  "message published on nats-stream")

	if err != nil {
		n.Logger.ErrorValues(map[string]interface{} {"subject": subject, "message": message, "error": err}, "fail try to publish message on [%s] => Error: %s", subject, err)
	}

	return err
}

func (n *StreamConnection) getDurableName(subject SubjectConfig) string {
	key := n.GetKey()
	if subject.IsQueueSubscription() {
		key = fmt.Sprintf("%s^%s", key, subject.Queue)
	}
	return key
}

func (n *StreamConnection) Subscribe(subject SubjectConfig, callback ReceiveMessageCallback, errorCallback ReceiveMessageErrorCallback) error {
	n.Logger.InfoValues(subject, "try to subscribe, requesting all available messages")

	//Starts from last message available
	startAt := stan.DeliverAllAvailable()

	//Queue name
	durableName := n.getDurableName(subject)
	subscribeId := stan.DurableName(durableName)

	var err error
	var subs stan.Subscription

	if subject.IsQueueSubscription() {
		subs, err = n.Conn.QueueSubscribe(subject.Name, subject.Queue, n.receiveMessage, startAt, subscribeId)
	} else {
		subs, err = n.Conn.Subscribe(subject.Name, n.receiveMessage, subscribeId, startAt)
	}

	if err != nil {
		return fmt.Errorf("error to subscribe subject on nats server [%s] => %s", n.ServerUrl, err)
	}

	n.Logger.Info("subscribed on subject [%s:%s] subscriptionId:%s", n.ServerUrl, subject.Name, durableName)

	info := StreamSubscriptionInfo{Subscription: subs}
	info.Callback = callback
	info.ErrorCallback = errorCallback
	info.Subject = subject
	n.Subscriptions[subject.Name] = info

	return err
}

func (n *StreamConnection) GetConnectionInfo() ConnectionInfo {
	return n.ConnectionInfo
}

func (n StreamConnection) GetSubscriptions() []SubscriptionInfo {
	var values []SubscriptionInfo

	for _, value := range n.Subscriptions {
		values = append(values, value.SubscriptionInfo)
	}

	return values
}

func (n *StreamConnection) receiveMessage(msg *stan.Msg) {
	var event BaseMessage
	subs := n.Subscriptions[msg.Subject]

	err := json.Unmarshal(msg.Data, &event)

	if err != nil {
		n.Logger.ErrorValues(msg,"error to try parser received message => %s", err)
	}

	event.Sequence = msg.Sequence
	event.Subject = msg.Subject
	event.Timestamp = msg.Timestamp

	err = subs.Callback(event)

	if err != nil {
		n.Logger.ErrorValues(event,"error to process received message => %s", err)
		n.HandleErrorProcessMessage(subs.SubscriptionInfo, event, err, n.Publish)

		if subs.ErrorCallback != nil {
			n.Logger.ErrorValues(event,"executing error callback")
			subs.ErrorCallback(subs.SubscriptionInfo, event, err)
		}
	}

	msg.Ack()

	subs.LastAck = time.Now()
	subs.LastSequence = msg.Sequence

	n.Subscriptions[msg.Subject] = subs
}