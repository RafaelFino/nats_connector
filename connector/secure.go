package nats_connector

import (
	"crypto/tls"
	"crypto/x509"
	"github.com/nats-io/go-nats"
	"io/ioutil"
)

type NatsSecureInfo struct {
	CertFile string
	KeyFile  string
	CAFile   string

	config *tls.Config
}

func (n *ConnectionInfo) IsTLS() bool {
	return n.TLSInfo.IsTLS()
}

func (n *NatsSecureInfo) IsTLS() bool {
	if  len(n.KeyFile) > 0 {
		return true
	}

	return false
}

func (n *BaseConnection) getConnOptions() ([]nats.Option) {
	var ret []nats.Option

	if n.IsTLS() {
		err := n.createTLSConfig()

		if err != nil {
			n.Logger.FatalValues(err, "error get TLS config")
			panic(err)
		}

		ret = append(ret, nats.Secure(n.TLSInfo.config))
	}

	return ret
}

func (n *BaseConnection) createTLSConfig() error {
	//Load cert
	cert, err := tls.LoadX509KeyPair(n.TLSInfo.CertFile, n.TLSInfo.KeyFile)
	if err != nil {
		n.Logger.FatalValues(err, "error parsing X509 certificate/key pair: %v", err)
		return err
	}

	if len(cert.Certificate) > 0 {
		cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
		if err != nil {
			n.Logger.FatalValues(err, "error parsing client certificate: %v", err)
			return err
		}
	}

	n.TLSInfo.config = &tls.Config{
		Certificates: 	[]tls.Certificate{cert},
		MinVersion: 	tls.VersionTLS12,
	}

	// Load CA cert
	if len(n.TLSInfo.CAFile) > 0 {
		caCert, err := ioutil.ReadFile(n.TLSInfo.CAFile)
		if err != nil {
			n.Logger.FatalValues(err, "error to try read caFile: %v", err)
			return err
		}

		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		n.TLSInfo.config.RootCAs = caCertPool
	}

	return err
}