package nats_connector

import (
	"github.com/nats-io/go-nats"
	"github.com/nats-io/go-nats-streaming"
	"time"
)

type ReceiveMessageCallback func(message BaseMessage) error
type ReceiveMessageErrorCallback func(subs SubscriptionInfo, message BaseMessage, err error)

type SubscriptionInfo struct {
	Callback      ReceiveMessageCallback
	ErrorCallback ReceiveMessageErrorCallback
	Subject       SubjectConfig
	LastSequence  uint64
	LastAck       time.Time
}

type SubjectConfig struct {
	Name       		string `json:"name"`
	DeadLetter 		string `json:"dead_letter,omitempty"`
	PanicOnError	bool   `json:"panic_on_error"`
	Queue         	string `json:"queue,omitempty"`
}

type NatsSubscriptionInfo struct {
	SubscriptionInfo
	Subscription *nats.Subscription
}

type StreamSubscriptionInfo struct {
	SubscriptionInfo
	Subscription stan.Subscription
}

func (s *SubjectConfig) IsQueueSubscription() bool {
	if len(s.Queue) > 0 {
		return true
	}

	return false
}

func (s *SubscriptionInfo) HasDeadLetterSubject() bool {
	if len(s.Subject.DeadLetter) > 0 {
		return true
	}

	return false
}