package nats_connector

import (
	log "c6bank.com/C6libs/logger"
	"encoding/json"
	"fmt"
	"github.com/nats-io/go-nats"
	"time"
)

var lastMessage uint64

func NewNatsConnector() *NatsConnection {
	ret := &NatsConnection{ Subscriptions:  make(map[string]NatsSubscriptionInfo)}
	ret.Logger = log.GetInstance()

	return ret
}

func (n *NatsConnection) Connect(serverUrl string) (*nats.Conn, error) {
	return n.ConnectTLS(serverUrl, NatsSecureInfo{})
}

func (n *NatsConnection) ConnectTLS(serverUrl string, tlsInfo NatsSecureInfo) (*nats.Conn, error) {
	nc, err := n.CreateNatsConnection(serverUrl, tlsInfo, n.handleConnectionLost)

	if err != nil {
		return nil, fmt.Errorf("error to connect on nats-stream server [serverUrl: %s] => %s", serverUrl, err)
	}

	n.TLSInfo = tlsInfo
	n.ServerUrl = serverUrl
	n.Conn = nc

	return nc, err
}

func (n *NatsConnection) Close() {
	n.Unsubscribe()
	n.Conn.Close()
}

func (n *NatsConnection) Unsubscribe() {
	for _, subs := range n.Subscriptions {
		subs.Subscription.Unsubscribe()
	}

	n.Subscriptions = nil
}

func (n *NatsConnection) UnsubscribeSubject(subject string) {
	for _, subs := range n.Subscriptions {
		subs.Subscription.Unsubscribe()
	}

	n.Subscriptions[subject].Subscription.Unsubscribe()
	delete(n.Subscriptions, subject)
}

func (n *NatsConnection) TryReconnect() {
	var err error

	n.Logger.Info("trying to reconnect on [%s]", n.ServerUrl)

	if n.IsTLS() {
		_, err = n.ConnectTLS(n.ServerUrl, n.TLSInfo)
	} else {
		_, err = n.Connect(n.ServerUrl)
	}

	if err != nil {
		n.Logger.ErrorValues(err,"fail to reconnect - Error: [%s]", err)
		panic(err)
	}
}

func (n *NatsConnection) handleConnectionLost(conn *nats.Conn) {
	n.Logger.Error("\n#### connection lost, trying to reconnect... ####\n\tConnection=> [%v]\n\n", conn)

	n.TryReconnect()

	for subject, subs := range n.Subscriptions {
		n.Logger.Info("trying to subscribe on subject [%s]", subject)

		err := n.Subscribe(subs.Subject, subs.Callback, subs.ErrorCallback)

		if err != nil {
			n.Logger.Error("fail try to subscribe on [%s] => Error: %s", subject, err)
		}
	}
}

func (n *NatsConnection) IsConnected() bool {
	return  n.Conn.IsConnected()
}

func (n *NatsConnection) IsConnecting() bool {
	status := n.Conn.Status()
	if status == nats.RECONNECTING || status == nats.CONNECTING {
		return true
	}
	return false
}

func (n *NatsConnection) checkConnection(wait bool) {
	conn := n.Conn
	if conn.IsConnected() {
		return
	}

	n.Logger.Warning("server is not connected...")

	if conn.IsClosed() {
		n.Logger.Debug("connection is closed! Trying to reconnect...")
		n.TryReconnect()
		n.checkConnection(wait)
	}

	if wait {
		for conn.IsReconnecting() {
			n.Logger.Debug("in reconnection state, waiting...")
			wait, _ := time.ParseDuration("1s")
			time.Sleep(wait)
		}

		n.checkConnection(wait)
	}
}

func (n *NatsConnection) Publish(subject string, message []byte) error {
	n.checkConnection(true)

	n.Logger.Debug("send [%s] => %s", subject, string(message))
	err := n.Conn.Publish(subject, message)

	if err != nil {
		n.Logger.Error("fail try to publish message on [%s] => Error: %s", subject, err)
	}

	n.Conn.Flush()

	n.Logger.DebugValues(map[string]interface{} {"subject": subject, "message-size": len(message)},  "message published on nats")

	return err
}

func (n *NatsConnection) Subscribe(subject SubjectConfig, callback ReceiveMessageCallback, errorCallback ReceiveMessageErrorCallback) error {
	n.Logger.InfoValues(subject, "try to subscribe")

	var subs *nats.Subscription
	var err error

	if subject.IsQueueSubscription() {
		subs, err = n.Conn.QueueSubscribe(subject.Name, subject.Queue, n.receiveMessage)
	} else {
		subs, err = n.Conn.Subscribe(subject.Name, n.receiveMessage)
	}

	if err != nil {
		return fmt.Errorf("error to subscribe subject on nats server [%s] => %s", n.ServerUrl, err)
	}

	n.Logger.Info("subscribe subject [%s => %s]", n.ServerUrl, subject.Name)

	info := NatsSubscriptionInfo{Subscription: subs}
	info.Callback = callback
	info.ErrorCallback = errorCallback
	info.Subject = subject
	n.Subscriptions[subject.Name] = info

	return nil
}

func (n *NatsConnection) GetConnectionInfo() ConnectionInfo {
	return n.ConnectionInfo
}

func (n NatsConnection) GetSubscriptions() []SubscriptionInfo {
	var values []SubscriptionInfo

	for _, value := range n.Subscriptions {
		values = append(values, value.SubscriptionInfo)
	}

	return values
}

func (n *NatsConnection) receiveMessage(msg *nats.Msg) {
	var event BaseMessage
	subs := n.Subscriptions[msg.Subject]

	err := json.Unmarshal(msg.Data, &event)

	if err != nil {
		n.Logger.ErrorValues(msg,"error to try parser received message => %s", err)
	}

	lastMessage++
	event.Sequence = lastMessage
	event.Subject = msg.Subject
	event.Timestamp = time.Now().Unix()

	err = subs.Callback(event)

	if err != nil {
		n.Logger.ErrorValues(event,"error to process received message => %s", err)
		n.HandleErrorProcessMessage(subs.SubscriptionInfo, event, err, n.Publish)

		if subs.ErrorCallback != nil {
			n.Logger.ErrorValues(event,"executing error callback")
			subs.ErrorCallback(subs.SubscriptionInfo, event, err)
		}
	}

	subs.LastAck = time.Now()
	subs.LastSequence = lastMessage

	n.Subscriptions[msg.Subject] = subs
}