package nats_connector

import (
	"fmt"
	"github.com/google/uuid"
)

type BaseMessage struct {
	Data      []interface{}
	Sequence  uint64
	Subject   string
	Timestamp int64
}

func NatsConnectionBuilderFromFile(path string) (ServerConnection, error) {
	info, err := CreateConnectionInfoFromFile(path)

	if err != nil {
		return nil, err
	}

	return NatsConnectionBuilder(info)
}

func NatsConnectionBuilder(info ConnectionInfo) (ServerConnection, error) {
	var ret ServerConnection
	var err error

	if len(info.StreamId) == 0 {
		server := NewNatsConnector()

		if info.IsTLS() {
			_, err = server.ConnectTLS(info.ServerUrl, info.TLSInfo)
		} else {
			_, err = server.Connect(info.ServerUrl)
		}

		ret = server
	} else {
		if len(info.ClientId) == 0 {
			info.ClientId = uuid.New().String()
		}

		server := NewStreamConnector()

		if info.IsTLS() {
			_, err = server.ConnectTLS(info.ServerUrl, info.StreamId, info.ClientId, info.TLSInfo)
		} else {
			_, err = server.Connect(info.ServerUrl, info.StreamId, info.ClientId)
		}

		ret = server
	}

	if err != nil {
		return nil, fmt.Errorf("error to connect on %s => %s", info.ServerUrl, err)
	}

	return ret, err
}
