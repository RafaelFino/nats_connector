#!/usr/bin/env bash
NATS_PATH=/opt/nats_server
NATS_BIN=gnatsd
NATS_URL=https://github.com/nats-io/gnatsd/releases/download/v1.2.0/gnatsd-v1.2.0-linux-amd64.zip

wget_install.sh "${NATS_BIN}" "${NATS_PATH}" "${NATS_URL}"