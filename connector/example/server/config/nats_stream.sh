
#!/usr/bin/env bash

NATS_PATH=/opt/nats_stream
NATS_BIN=nats-streaming-server
NATS_URL=https://github.com/nats-io/nats-streaming-server/releases/download/v0.10.2/nats-streaming-server-v0.10.2-linux-amd64.zip
NATS_CONFIG=$1

echo ./wget_install.sh "${NATS_BIN}" "${NATS_PATH}" "${NATS_URL}"
wget_install.sh "${NATS_BIN}" "${NATS_PATH}" "${NATS_URL}"

echo "${NATS_PATH}/${NATS_BIN}" -sc "${NATS_CONFIG}"
"${NATS_PATH}/${NATS_BIN}" -sc "${NATS_CONFIG}"