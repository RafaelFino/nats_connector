#!/usr/bin/env bash

NATS_PATH=/opt/nats_server
NATS_BIN=gnatsd
NATS_URL=https://github.com/nats-io/gnatsd/releases/download/v1.2.0/gnatsd-v1.2.0-linux-amd64.zip
NATS_CONFIG=$1

echo ./wget_install.sh "${NATS_BIN}" "${NATS_PATH}" "${NATS_URL}"
wget_install.sh "${NATS_BIN}" "${NATS_PATH}" "${NATS_URL}"

echo "${NATS_PATH}/${NATS_BIN}" -c "${NATS_CONFIG}" -D
"${NATS_PATH}/${NATS_BIN}" -c "${NATS_CONFIG}" -D