#!/usr/bin/env bash
NATS_PATH=/opt/nats_stream
NATS_BIN=nats-streaming-server
NATS_URL=https://github.com/nats-io/nats-streaming-server/releases/download/v0.10.2/nats-streaming-server-v0.10.2-linux-amd64.zip

wget_install.sh "${NATS_BIN}" "${NATS_PATH}" "${NATS_URL}"
"${NATS_PATH}/${NATS_BIN}" -p 4242 -cid nats-cluster -cluster_bootstrap -cluster nats-route://stream_server:5242 -routes nats-route://nats_server:5222