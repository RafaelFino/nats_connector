#!/usr/bin/env bash
BIN_NAME=$1
TARGET_DIR=$2
SOURCE_URL=$3

if [ ! -d "${TARGET_DIR}" ]; then
  mkdir "${TARGET_DIR}"
fi

if [ ! -f "${TARGET_DIR}/${BIN_NAME}" ]; then
    if [ ! -f "${TARGET_DIR}/${BIN_NAME}.zip" ]; then
        rm -rf "${TARGET_DIR}/${BIN_NAME}.zip"
    fi

    yum install -y wget unzip
    wget "${SOURCE_URL}" -O "${TARGET_DIR}/${BIN_NAME}.zip"
    unzip -j "${TARGET_DIR}/${BIN_NAME}.zip" -d "${TARGET_DIR}"

    rm -rf "${TARGET_DIR}/${BIN_NAME}.zip"
fi