package main

import (
	log "c6bank.com/C6libs/logger"
	nats "c6bank.com/C6libs/nats_connector"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

type ClientType int

const (
	PUB ClientType = iota
	SUB
)

var ClientTypes = map[string]ClientType{
	"PUB":   PUB,
	"SUB":   SUB,
}

type ClientConfig struct {
	Count      int
	Sleep      time.Duration
	ClientType ClientType
	Subject    string
	ConfigPath string
}

type ExampleMessage struct {
	Name  string
	Value int
	When  time.Time
	Pos int
}

var logger *log.Logger

func main() {
	log.Init(os.Args[0])
	logger = log.GetInstance()
	logger.Info("[main] Starting...")

	config := ParseArgs(os.Args)

	logger.DebugValues(config, "[main] Config")

	connectionInfo, err := nats.CreateConnectionInfoFromFile(config.ConfigPath)

	if err != nil {
		panic(err)
	}

	conn, err := nats.NatsConnectionBuilder(connectionInfo)

	if err != nil {
		panic(err)
	}

	defer conn.Close()

	switch config.ClientType {
	case PUB:
		go func(conn nats.ServerConnection, subject string, messageCount int, sleep time.Duration) {
			for true {
				randomValue := rand.New(rand.NewSource(time.Now().UnixNano()))

				for i := 0; i < messageCount; i++ {
					message := []ExampleMessage{{Name: fmt.Sprintf("Pos_%d", i), Value: randomValue.Intn(1000), When: time.Now(), Pos: i}}

					rawData, _ := json.Marshal(message)

					err := conn.Publish(subject, rawData)

					if err != nil {
						logger.Error("## [pub] Error to try publish: %s", err)
					}
				}

				time.Sleep(sleep)
			}
		} (conn, config.Subject, config.Count, config.Sleep)
		break
	case SUB:
		conn.Subscribe(nats.SubjectConfig{Name: config.Subject}, ProcessMessage, ErrorProcessMessage)
		break
	default:
		logger.Error("[main] ClientType is invalid: %v", config)
	}

	logger.Info("[main] Connected! Status: %s", conn.IsConnected())

	runtime.Goexit()
	logger.Info("[main] runtime exit...")
}

func ProcessMessage(message nats.BaseMessage) error {
	logger := log.GetInstance()
	logger.InfoValues(message, "message received")

	return nil
}

func ErrorProcessMessage(subs nats.SubscriptionInfo, message nats.BaseMessage, err error) {
	logger := log.GetInstance()
	logger.ErrorValues([]interface{} { subs, message, err}, "Error to try process received message")
}

func ParseArgs(args []string) ClientConfig {
	sleep, _ := time.ParseDuration("1s")
	argsMap := map[string]string{
		"SLEEP":            "1",
		"COUNT":            "1",
		"CLIENT_TYPE":      "SUB",
		"SUBJECT":     		"TEST_SUBJECT",
		"CONFIG_PATH":		"nats-config.json",
	}

	for pos, arg := range args {
		if arg[0] == '-' {
			arg = arg[1:]
		}
		params := strings.Split(arg, "=")

		if len(params) > 1 {
			key := strings.ToUpper(params[0])
			logger.Debug("[ParseArgs] Argument[%d] => { %s: %s}", pos, key, params[1])
			argsMap[key] = params[1]
		}
	}

	sleep, _ = time.ParseDuration(fmt.Sprintf("%ss", argsMap["SLEEP"]))
	count, _ := strconv.Atoi(argsMap["COUNT"])

	return ClientConfig{
		Sleep:      sleep,
		Count:      count,
		ClientType: ClientTypes[argsMap["CLIENT_TYPE"]],
		Subject:    argsMap["SUBJECT"],
		ConfigPath: argsMap["CONFIG_PATH"],
	}
}
