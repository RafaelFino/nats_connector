package main

import (
	nats "c6bank.com/C6libs/nats_connector"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	config := nats.ConnectionInfo{}

	config.ClientId  = "clientUniqueId"
	config.ServerUrl = "ntas://nats-server:4222"
	config.StreamId = "stream-server"
	config.TLSInfo.CAFile = "/keys/ca.pem"
	config.TLSInfo.CertFile = "/keys/certificate.crt"
	config.TLSInfo.KeyFile = "/keys/nats_client.pem"

	raw, err := json.MarshalIndent(config, "", "\t")

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(os.Args[1], raw, 0777)
	if err != nil {
		panic(err)
	}

	fmt.Printf("File wrote on %s\n", os.Args[1])
	fmt.Println(string(raw))
}