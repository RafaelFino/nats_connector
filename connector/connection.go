package nats_connector

import (
	log "c6bank.com/C6libs/logger"
	"encoding/json"
	"fmt"
	"github.com/nats-io/go-nats"
	"github.com/nats-io/go-nats-streaming"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

type ConnectionInfo struct {
	ClusterInfo					`json:"cluster,omitempty"`
	ServerUrl 	string 			`json:"server_url"`
	TLSInfo		NatsSecureInfo	`json:"tls,omitempty"`
}

type ClusterInfo struct {
	StreamId string `json:"stream_id"`
	ClientId string `json:"client_id,omitempty"`
}

type ServerConnection interface {
	Close()
	Unsubscribe()
	IsConnected() bool
	IsConnecting() bool
	TryReconnect()

	GetConnectionInfo() ConnectionInfo
	GetSubscriptions() []SubscriptionInfo

	Publish(subject string, message []byte) error
	Subscribe(subject SubjectConfig, callback ReceiveMessageCallback, errorCallback ReceiveMessageErrorCallback) error
}

type BaseConnection struct {
	ConnectionInfo
	Logger    	  *log.Logger
}

type NatsConnection struct {
	BaseConnection
	Conn          *nats.Conn
	Subscriptions map[string]NatsSubscriptionInfo
}

type StreamConnection struct {
	BaseConnection
	Conn          	stan.Conn
	Subscriptions 	map[string]StreamSubscriptionInfo
}

func CreateConnectionInfoFromFile(path string) (ConnectionInfo, error) {
	var config ConnectionInfo

	raw, err := ioutil.ReadFile(path)

	if err != nil {
		return config, err
	}

	err = json.Unmarshal(raw, &config)

	return config, err
}

func (n *ConnectionInfo) GetKey() string {
	return strings.Join([]string {n.ServerUrl, n.StreamId, n.ClientId}, "^")
}

func (n *ConnectionInfo) SaveToFile(path string) error {
	data, err := json.MarshalIndent(n, "", "\t")

	if err != nil {
		return err
	}

	return ioutil.WriteFile(path, data, 0644)
}

func (n *BaseConnection) handleError(conn *nats.Conn, subscription *nats.Subscription, err error) {
	vars := map[string]interface{} { "conn": conn, "subscription": subscription, "error": err}
	n.Logger.ErrorValues(vars,"nats-server error")
}

func (n *BaseConnection) handleReconnect(conn *nats.Conn) {
	n.Logger.InfoValues(conn,"nats-server reconnected")
}

func (n *BaseConnection) handleClose(conn *nats.Conn) {
	n.Logger.InfoValues(conn,"nats-server close")
}

func (n *BaseConnection) handleDisconnect(conn *nats.Conn) {
	n.Logger.InfoValues(conn,"nats-server disconnected")
}

func (n *BaseConnection) handleNewServer(nc *nats.Conn) {
	servers := map[string]interface{} { "servers":nc.Servers(), "discovered": nc.DiscoveredServers()}
	n.Logger.InfoValues(servers, "Change on servers list")
}

func (n *BaseConnection) CreateNatsConnection(serverUrl string, tlsInfo NatsSecureInfo, connectionLostHandler nats.ConnHandler) (*nats.Conn, error) {
	n.Logger.Debug("try to connect on nats-server [%s]", serverUrl)

	opts := []nats.Option{
		nats.ReconnectHandler(connectionLostHandler),
		nats.ErrorHandler(n.handleError),
		nats.DiscoveredServersHandler(n.handleNewServer),
		nats.ReconnectHandler(n.handleReconnect),
		nats.ClosedHandler(n.handleClose),
		nats.DisconnectHandler(n.handleDisconnect),
		nats.Timeout(10 * time.Second),
	}

	var err error
	var nc *nats.Conn

	n.TLSInfo = tlsInfo
	opts = append(opts, n.getConnOptions()...)

	nc, err = nats.Connect(serverUrl, opts...)

	if err != nil {
		return nil, fmt.Errorf("error to connect on nats server [serverUrl: %s] => %s", serverUrl, err)
	}

	n.Logger.Debug("connected on nats-server [serverUrl: %s]", serverUrl)
	n.ServerUrl = serverUrl

	return nc, err
}

func (n *BaseConnection) HandleErrorProcessMessage (subs SubscriptionInfo, message BaseMessage, messageErr error, publisher func(string, []byte) error) {
	n.Logger.InfoValues(map[string]interface{} {"Subscription": subs, "Message": message, "Error": messageErr}, "fail to try process message")

	if subs.HasDeadLetterSubject() {
		raw, _ := json.Marshal(message)
		err := publisher(subs.Subject.DeadLetter, raw)

		if err != nil {
			n.Logger.ErrorValues(map[string]interface{} {"Subscription": subs, "Message": message, "MessageError": messageErr, "SendError": err}, "fail to try send error message to dead letter subject")
		}
	} else {
		n.Logger.InfoValues(map[string]interface{} {"Subscription": subs, "Message": message, "Error": messageErr}, "no dead letter subject on subscription config")
	}

	if subs.Subject.PanicOnError {
		n.Logger.FatalValues(message,"error to receive message => %s, aborting!", messageErr)
		panic(messageErr)
	}
}

func CreateReceiver(connInfo ConnectionInfo, subjects []SubjectConfig, callback ReceiveMessageCallback, handleErrorReceivedMessage ReceiveMessageErrorCallback) ServerConnection {
	logger := log.GetInstance()
	conn, err := NatsConnectionBuilder(connInfo)

	for err != nil {
		logger.ErrorValues(map[string]interface{}{"args": os.Args, "error": err}, "fail to try connect on nats server: %s", err)
		panic(err)
	}

	for _, subject := range subjects {
		err = conn.Subscribe(subject, callback, handleErrorReceivedMessage)

		for err != nil {
			logger.ErrorValues(map[string]interface{}{"args": os.Args, "subject": subject, "error": err}, "fail to try subscribe subject: %s", err)
			panic(err)
		}
	}

	if err != nil {
		logger.Error("fail to try subscribe on nats server %s", err)
		panic(err)
	}

	return conn
}