# nats_connector

A simple client connector to NATS Server including the native and the Stream protocol.

## [NATS](https://nats.io/)
NATS is an open-source, cloud-native messaging system. Companies like Apcera, Baidu, Siemens, VMware, HTC, and Ericsson rely on NATS for its highly performant and resilient messaging capabilities.

### [Native protocol](https://nats.io/documentation/internals/nats-protocol/)
The NATS wire protocol is a simple, text-based publish/subscribe style protocol. Clients connect to and communicate with gnatsd (the NATS server) through a regular TCP/IP socket using a small set of protocol operations that are terminated by newline.

Unlike traditional messaging systems that use a binary message format that require an API to consume, the text-based NATS protocol makes it easy to implement clients in a wide variety of programming and scripting languages.

### [Stream server and protocol](https://nats.io/documentation/streaming/nats-streaming-intro/)
NATS Streaming is a data streaming system powered by NATS, and written in the Go programming language. The executable name for the NATS Streaming server is nats-streaming-server. NATS Streaming embeds, extends, and interoperates seamlessly with the core NATS platform. The NATS Streaming server is provided as open source software under the Apache-2.0 license. Synadia actively maintains and supports the NATS Streaming server.

## Why this connector?
This client implements connections events to simplify and made transparent behavior for client applications, creating a resilient transport layer between nats server and client


License
----

MIT
